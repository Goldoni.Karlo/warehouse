var mongoose = require('mongoose');

var postsSchema = new mongoose.Schema({
  title: String,
  description: String,
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'users'}
  });

// Requires population of author
postsSchema.methods.toJSONFor = function(user){
  return {
    id: this._id,
    body: this.body,
    createdAt: this.createdAt
    };
};

mongoose.model('posts', postsSchema);
