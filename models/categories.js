var mongoose = require('mongoose');

var categoriesSchema = new mongoose.Schema({
  name: String,
  });

// Requires population of author
categoriesSchema.methods.toJSONFor = function(user){
  return {
    id: this._id,
    body: this.body,
    createdAt: this.createdAt
    };
};

mongoose.model('categories', categoriesSchema);
