var mongoose = require('mongoose');

var carsSchema = new mongoose.Schema({
  name: String,
  modelname: String,
  });

// Requires population of author
carsSchema.methods.toJSONFor = function(user){
  return {
    id: this._id,
    body: this.body,
    createdAt: this.createdAt
    };
};

mongoose.model('cars', carsSchema);
