var mongoose = require('mongoose');

var productsSchema = new mongoose.Schema({
  name: String,
  category: {type: mongoose.Schema.Types.ObjectId, ref: 'categories'}
  });

// Requires population of author
productsSchema.methods.toJSONFor = function(user){
  return {
    id: this._id,
    body: this.body,
    createdAt: this.createdAt
    };
};

mongoose.model('products', productsSchema);
