var mongoose = require('mongoose');
var router = require('express').Router();
const categories = mongoose.model('categories');
const products = mongoose.model('products');

function getCategoryName(id, cb) {
    categories.findOne({
        _id: id
    }, function (err, cat) {
        if (err) {
            console.log('Error searching category by id', err);
            cb(err);
        } else {
            if (cat == undefined) {
                cb(false, 'Category for this product was deleted');
            } else {
                cb(false, cat.name);
            }
        }
    });
}


router.get('/', function (req, res, next) {
    console.log('Query=', req.query);
    products.find(req.query, function (err, prods) {
        if (err) {
            console.log('Error products/get', err);
        } else {
            if (prods.length == 0) {
                console.log('Nothing');
                res.status(404).send('Nothing search for query');
            } else {
                console.log('Search', prods);
                console.log('Search', prods.length);
                req.prodlist = prods;
                next();
            }
        }
    });
}, function addCategoryName(req, res, next) {
    // Add CategoryName to every product in productlist
    var Nexter = req.prodlist.length;
    for (let i = 0; i < req.prodlist.length; i++) {
        getCategoryName(req.prodlist[i].category, function (err, result) {
            if (err) {
                console.log('Error getting category name', err);
            } else {
                console.log('Catname[' + i + ']=' + result);
                req.prodlist[i].categoryname = result;
                Nexter--;
                if (Nexter == 0) {
                    console.log('Callbacks left to wait =', Nexter);
                    next();
                }
            }
        });
    }
}, function output(req, res) {
    res.render('products', {
        products: req.prodlist
    });
});


router.get('/:id', function (req, res, next) {
    products.find({
        _id: req.params.id
    }, function (err, prods) {
        if (err) {
            console.log('Error products/get', err);
        } else {
            if (prods.length == 0) {
                console.log('Nothing');
                res.status(404).send('Object with _id not exist');
            } else {
                console.log('Search', prods);
                console.log('Search', prods.length);
                req.prodlist = prods;
                next();
            }
        }
    });
}, function addCategoryName(req, res, next) {
    // Add CategoryName to every product in productlist
    var Nexter = req.prodlist.length;
    for (let i = 0; i < req.prodlist.length; i++) {
        getCategoryName(req.prodlist[i].category, function (err, result) {
            if (err) {
                console.log('Error getting category name', err);
            } else {
                //console.log('Catname['+i+']='+result);
                req.prodlist[i].categoryname = result;
                Nexter--;
                if (Nexter == 0) {
                    //console.log('Callbacks left to wait =', Nexter);
                    next();
                }
            }
        });
    }
}, function output(req, res) {
    res.render('product', {
        products: req.prodlist
    });
    //res.render('productObj',  {product:req.prodlist[0]}); 
});


router.post('/', function (req, res, next) {
    // Create new product from body etc we have body {name:'value'}
    // if you need REST API create {name:'value'}  from req.params....
    console.log('POST/categories body=', req.body);
    console.log('POST/categories body.name=', req.body.name)
    next();
}, function reqcheck(req, res, next) {
    // Check name and id issue in request
    let errstr = '';
    console.log('name', req.body.name);
    console.log('id category', req.body.category);
    if (req.body.name == '') {
        errstr += 'name is empty ';
    }
    if (req.body.category == undefined) {
        errstr += ' id for category of product is undefined ';
    }
    if (!errstr) {
        next();
    } else {
        res.status(404).send(errstr);
    }
}, function nameUniqueCheck(req, res, next) {
    // Check name is unique
    products.findOne({
        name: req.body.name
    }, function (err, cat) {
        if (err) {
            console.log('err checking unique of categorie name', err);
        } else {
            if (cat == undefined) {
                console.log('Undefined', cat);
                next();
            } else {
                console.log('Уже есть', cat);
                res.status(404).send('This name is allready used! Can not create new!');
                console.log(cat, 'Name for new productis not unique!');
            }
        }
    });
}, function idCategoryCheck(req, res, next) {
    // Check id of product category is real
    getCategoryName(req.body.category, function (err, result) {
        if (err) {
            console.log('Error getting category name', err);
        } else {
            if (result == 'Category for this product was deleted') {
                res.status(404).send('Category for this product is not exist! Can not create new product');
                console.log(result, 'Category of new product is not exist!');
                console.log('Category for product=' + result);
            } else {
                req.categoryname = result;
                console.log('Creating category');
                next();
            }
        }
    });
}, function createProduct(req, res) {
    // Create new categorie in MongoDB 
    const pro = new products({
        name: req.body.name,
        category: req.body.category
    });
    pro.save((err, cat) => {
        if (err) {
            console.log('err', err);
        } else {
            console.log(cat, 'New product is created from routs/products');
            //res.status(201).send(cat);
            res.redirect('http://localhost:3001/categories/' + req.body.category);
        }
    });
});




router.delete('/:_id', function (req, res, next) {
    console.log('Deletion category with id=', req.params._id);
    next();
}, function idreqreal(req, res, next) {
    // Check id is real in request
    products.findOne({
        _id: req.params._id
    }, function (err, catid) {
        if (catid == undefined) {
            res.status(404).send('No such product Object in MongoDB for deleting');
        } else {
            req.body.category = catid.category;
            products.findOneAndDelete({
                _id: req.params._id
            }, function (err, cates) {
                if (err) {
                    console.log('err deleting from routes/cars', err);
                } else {
                    console.log(cates, 'Object deleted from routes/cars');
                    //res.status(201).send('Category deleted succesfully');
                    res.redirect('http://localhost:3001/categories/' + req.body.category);
                }
            });
        }
    });
});


router.patch('/:_id', function (req, res, next) {
    //  Get product_id on input
    console.log('Object for update (patch)=', req.params._id);
    console.log('Parameters for update (patch)=', req.body);
    next();
}, function idreqreal(req, res, next) {
    // Check id is real in request
    products.findOne({
        _id: req.params._id
    }, function (err, catid) {
        if (catid == undefined) {
            res.status(404).send('No such product Object in MongoDB for patching (updating)');
        } else {
            products.findOneAndUpdate({
                _id: req.params._id
            }, {
                $set: req.body
            }, function (err, cates) {
                if (err) {
                    console.log('error patching (updating) from routes/products', err);
                } else {
                    console.log(cates, 'Object updated from routes/products');
                    //res.status(201).send('Category updated succesfully');
                    res.redirect('http://localhost:3001/categories/' + req.body.category);
                }
            });
        }
    });
});

router.patch('/', function (req, res, next) {
    //  Get product_id on input
    console.log('Object for updatein query=', req.query);
    console.log('Parameters for update (patch)_id=', req.query._id);
    req._id = req.query._id;
    next();
}, function idreqreal(req, res, next) {
    // Check id is real in request
    products.findOne({
        _id: req.query._id
    }, function (err, prodid) {
        if (prodid == undefined) {
            res.status(404).send('No such product Object in MongoDB for patching (updating)');
        } else {
            console.log('Must update', prodid);
            req.product = prodid;
            next();
        }
    })
}, function getDataForPath(req, res) {
    console.log('Updating for', req.product);
    res.render('productpatch', {
        product: req.product
    })

});


module.exports = router;
