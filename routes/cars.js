var mongoose = require('mongoose');
var router = require('express').Router();
const cars = mongoose.model('cars');

router.get('/', function (req, res) {
    console.log('Query=', req.query);
    cars.find(req.query, function (err, cars) {
        res.render('cars', {
            cars
        });
    });

});


router.post('/', function (req, res, next) {
    console.log('car post', req.query);
    next();

}, function reqcheck(req, res, next) {
    // Check name and modelname are in request
    let errstr = '';
    if (req.query.name == undefined) {
        errstr += 'name is undefined ';
    }
    if (req.query.modelname == undefined) {
        errstr += 'modelname is undefined ';
    }
    if (!errstr) {
        next();
    } else {
        res.status(404).send(errstr);
    }
}, function create(req, res) {
    // Create new car in MongoDB 
    const car = new cars({
        name: req.query.name,
        modelname: req.query.modelname
    });
    car.save((err, car) => {
        if (err) {
            console.log('err', err);
        } else {
            console.log(car, 'Object saved from routes/cars');
            res.status(201).send(car);
        }
    });
});


router.delete('/:_id', function (req, res, next) {
    console.log('Deletion id=', req.params._id);
    next();
}, function idreqreal(req, res, next) {
    // Check id is real in request
    cars.findOne({
        _id: req.params._id
    }, function (err, carid) {
        if (carid == undefined) {
            res.status(404).send('No such Object in MongoDB for deleting');
        } else {
            cars.findOneAndDelete({
                _id: req.params._id
            }, function (err, carres) {
                if (err) {
                    console.log('err deleting from routes/cars', err);
                } else {
                    console.log(carres, 'Object deleted from routes/cars');
                    res.status(201).send('Car deleted succesfully');
                }
            });
        }
    });
});


router.patch('/:_id', function (req, res, next) {
    //  Get car_id on input
    console.log('Object car for update (patch)=', req.params._id);
    console.log('Parameters for update (patch)=', req.body);
    next();
}, function idreqreal(req, res, next) {
    // Check id is real in request
    cars.findOne({
        _id: req.params._id
    }, function (err, carid) {
        if (carid == undefined) {
            res.status(404).send('No such Object in MongoDB for patching (updating)');
        } else {
            console.log('Install =', req.body);
            cars.findOneAndUpdate({
                _id: req.params._id
            }, {
                $set: req.body
            }, function (err, carres) {
                if (err) {
                    console.log('error patching (updating) from routes/cars', err);
                } else {
                    console.log(carres, 'Object updated from routes/cars');
                    res.status(201).send('Car updated succesfully');
                }
            });
        }
    });
});


router.put('/:_id', function (req, res, next) {
    //  Get car_id on input
    console.log('Object car for update (patch)=', req.params._id);
    console.log('Parameters for update (patch)=', req.body);
    next();
}, function idreqreal(req, res, next) {
    // Check id is real in request
    cars.findOne({
        _id: req.params._id
    }, function (err, carid) {
        if (carid == undefined) {
            res.status(404).send('No such Object in MongoDB for put (replace)');
        } else {
            console.log('Install =', req.body);
            cars.replaceOne({
                _id: req.params._id
            }, req.body, function (err, carres) {
                if (err) {
                    console.log('error putting (replacing) from routes/cars', err);
                } else {
                    console.log(carres, 'Object replaced from routes/cars');
                    res.status(201).send('Car replaced succesfully');
                }
            });
        }
    });
});





module.exports = router;
