var mongoose = require('mongoose');
var router = require('express').Router();
const users = mongoose.model('users');

router.get('/', function (req, res) {
    users.find(function (err, users) {
        res.render('users', {
            users
        });
    })
});


router.get('/:id', function (req, res) {
    users.findById(req.params.id, function (err, user) {
        res.render('user', {
            user: user
        });
    });
});

module.exports = router;
