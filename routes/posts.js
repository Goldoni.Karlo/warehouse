var mongoose = require('mongoose');
var router = require('express').Router();

const posts = mongoose.model('posts');
const users = mongoose.model('users');

router.get('/', function (req, res) {
    posts.find(function (err, posts) {
        res.render('posts', {
            posts
        });
    })
});

router.get('/:id', function (req, res) {
    posts.findById(req.params.id, function (err, posts) {
        users.findById(posts.user, function (err, users) {
            res.render('post', {
                posts: posts,
                author: users
            });
        });
    });
});

module.exports = router;
