var router = require('express').Router();
router.use('/categories', require('./categories'));
router.use('/products', require('./products'));
router.use('/users', require('./users'));
router.use('/cars', require('./cars'));
router.use('/posts', require('./posts'));
module.exports = router;
